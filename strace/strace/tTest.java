package strace;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.apache.commons.math3.stat.inference.TTest;

public class tTest {

	/**
	 * @param args
	 * @throws ParseException 
	*/
	
	private static Map<String,String[]> syscallDesc = new HashMap<String,String[]>();
	public static String result;
	public String htmlString;
	
	
    tTest(String vers1, String vers2)
	{
		//String vers2 = "a6c50aef0b8bf5a75bf7cd7a53a927ba3af2e9fd";
		//String vers1 = "25ecfb92aab381f19a34c45dd4684134651b03ba";
		htmlString="";
		initializeMap();
		try {
			htmlString+=doTTest(vers1,vers2);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
    
    public String getReport()
    {
    	return htmlString;
    }

	
	public static void initializeMap()
	{
		syscallDesc.put("_llseek",new String[] {"You are performing more number of edits in your file", "You are performing less number of edits in your file "});
		syscallDesc.put("restart_syscall",new String[] {"You are using more threads","You are using less number of threads"});
		syscallDesc.put("access",new String[] {"You are accessing more number of files", "You are accessing less number of files "});
		syscallDesc.put("brk",new String[] {"You are probably using more variables and/or more memory", "You are probably using less variables and/or less memory "});
		syscallDesc.put("cacheflush",new String[] {"You are performing more calculations or using more memory", "You are performing less calculations or using less memory"});
		syscallDesc.put("chmod",new String[] {"You are probably accessing more files", "You are probably accessing less files"});
		syscallDesc.put("clock_gettime",new String[] {"You are probably performing more number of calculations","You are probably performing less number of calculations"});
		syscallDesc.put("clone",new String[] {"You are using more threads","You are using less number of threads"});
		syscallDesc.put("close",new String[] {"You are doing more number of file operations", "You are doing less number of file operations"});
		syscallDesc.put("epoll_wait",new String[] {"You are doing more number of I/O operations", "You are doing less number of I/O operations "});
		syscallDesc.put("fcntl64",new String[] {"You are doing more number of file operations", "You are doing less number of file operations"});
		syscallDesc.put("fstat64",new String[] {"You are doing more number of file operations", "You are doing less number of file operations"});
		syscallDesc.put("fsync",new String[] {"You are doing more number of editing operations to files", "You are doing less number of editing operations to files"});
		syscallDesc.put("ftruncate",new String[] {"You are doing more number of editing operations to files", "You are doing less number of editing operations to files"});
		syscallDesc.put("futex",new String[] {"You are probably using more memory/threads", "You are probably using less memory/threads"});
		syscallDesc.put("getdents64",new String[] {"Not much advice here, probably you are using more file operations", "Not much advice here, probably you are using less file operations"});
		syscallDesc.put("getegid32",new String[] {"You are probably using more file operations", "You are probably using less file operations"});
		syscallDesc.put("geteuid32",new String[] {"You are probably using more file operations", "You are probably using less file operations"});
		syscallDesc.put("getgid32",new String[] {"You are probably using more file operations", "You are probably using less file operations"});
		syscallDesc.put("getpid",new String[] {"You are using more threads","You are using less number of threads"});
		syscallDesc.put("getpriority",new String[] {"You are probably doing more CPU intensive operations", "You are probably doing less CPU intensive operations"	});
		syscallDesc.put("getsockname",new String[] {"You are doing more  network related operations using Database/Web", "You are doing less network related operations using Database/Web"	});
		syscallDesc.put("gettid",new String[] {"You are using more threads","You are using less number of threads"});
		syscallDesc.put("gettimeofday",new String[] {"You are using more calculations or accessing systime more","You are using less calculations or accessing systime less"});
		syscallDesc.put("getuid32",new String[] {"You are probably using more file operations", "You are probably using less file operations"});
		syscallDesc.put("ioctl",new String[] {"You are probably using more file operations", "You are probably using less file operations"});
		syscallDesc.put("lseek",new String[] {"You are probably using more file/memory operations", "You are probably using less file/memory operations"});
		syscallDesc.put("lstat64",new String[] {"You are using more file access", "You are using less file access"});
		syscallDesc.put("madvise",new String[] {"You are probably using more file/memory operations", "You are probably using less file/memory operations"});
		syscallDesc.put("mkdir",new String[] {"You are  using more file/memory operations", "You are  using less file/memory operations"});
		syscallDesc.put("mmap2",new String[] {"You are  using more memory operations", "You are  using less memory operations"});
		syscallDesc.put("mprotect",new String[] {"You are  using more memory operations", "You are  using less memory operations"});
		syscallDesc.put("munmap",new String[] {"You are  using more memory operations", "You are  using less memory operations"});
		syscallDesc.put("open",new String[] {"You are  opening more often", "You are opening less often"});
		syscallDesc.put("pipe",new String[] {"You are more intra-process communication", "You are less intra-process communication"	});
		syscallDesc.put("poll",new String[] {"You are more I/O communication", "You are less I/O communication"	});
		syscallDesc.put("read",new String[] {"You are performing more reading operation on files", "You are performing less reading operation on files"});
		syscallDesc.put("recvfrom",new String[] {"You are doing more  network related operations using Web", "You are doing less network related operations using 		Web"});
		syscallDesc.put("recvmsg",new String[] {"You are doing more  network related operations using Database/Web", "You are doing less network related operations using 		Database/Web"});
		syscallDesc.put("sendmsg",new String[] {"You are doing more  network related operations using Database/Web", "You are doing less network related operations using 		Database/Web"});
		syscallDesc.put("sendto",new String[] {"You are doing more  network related operations using Database/Web", "You are doing less network related operations using 		Database/Web"});
		syscallDesc.put("set_tls",new String[] {"No information available","No information available"});
		syscallDesc.put("setsockopt",new String[] {"You are doing more  network related operations using Database/Web", "You are doing less network related operations using 		Database/Web"});
		syscallDesc.put("sigaction",new String[] {"You are possibly using more threads", "You are possibly using less threads"});
		syscallDesc.put("sigprocmask",new String[] {"You are possibly using more threads", "You are possibly using less threads"});
		syscallDesc.put("socket",new String[] {"You are doing more  network related operations using Database/Web", "You are doing less network related operations using 		Database/Web"});
		syscallDesc.put("stat64",new String[] {"You are probably using more file or I/O operations", "You are probably using less file or I/O operations"});
		syscallDesc.put("total",new String[] {"Your sum  of system calls is more for this version", "Your sum  of system calls is less for this version"	});
		syscallDesc.put("unlink",new String[] {"You are using more file access", "You are using less file access"});
		syscallDesc.put("write",new String[] {"You are  writing more often", "You are writing less often"	});
		syscallDesc.put("writev",new String[] {"You are  writing more often", "You are writing less often"	});
		
		
		
	}
	
	public static boolean ttest(final double[] sample1, final double[] sample2,double alpha)
	{
		TTest T_TEST = new TTest();
		return T_TEST.pairedTTest(sample1, sample2, alpha);
	}
	
	public static double ttest(final double[] sample1, final double[] sample2)
	{
		TTest T_TEST = new TTest();
		return T_TEST.pairedTTest(sample1, sample2);
	}
	
	
	public static String doTTest(String verID1, String verID2) throws ParseException
	{
		database db = new database();
		List<String> version1Tests = db.getVersionTests(verID1);
		List<String> version2Tests = db.getVersionTests(verID2);
		List<String> syscalls = new ArrayList<String>();
		Set<String> keys = new HashSet();
		List<JSONObject> vers1Tests = new ArrayList<JSONObject>();
		List<JSONObject> vers2Tests = new ArrayList<JSONObject>();

		
		int noTestsVers1 = version1Tests.size();
		int noTestsVers2 = version2Tests.size();
		
		if(Math.min(noTestsVers1, noTestsVers2)<pathVariables.minTests)
		{
			System.out.println("Insufficient number of tests, not possible to comment on Power");
			return "";
		}
		
		int testNoDiff = noTestsVers1-noTestsVers2;
		//System.out.println(testNoDiff);
		
		for(String test: version1Tests)
		{
			if(testNoDiff>0)
			{
				testNoDiff--;
				continue;
			}
			JSONObject jsonObject = (JSONObject) new JSONParser().parse(test);
			vers1Tests.add(jsonObject);
			//System.out.println("Version 1 :: "+jsonObject);
			keys.addAll(jsonObject.keySet());
		}
		
		
		for(String test: version2Tests)
		{
			if(testNoDiff<0)
			{
				testNoDiff++;
				continue;
			}
			
			JSONObject jsonObject = (JSONObject) new JSONParser().parse(test);
			vers2Tests.add(jsonObject);
			//System.out.println("Version 2 :: "+jsonObject);
			keys.addAll(jsonObject.keySet());
		}
		
		Map<String, Double> sysCalls = new TreeMap<String, Double>();
		
		double[] percentageChange = new double[keys.size()];
		boolean[] significance = new boolean[keys.size()];
		
		double[] vers1sample = new double[vers1Tests.size()];
		double[] vers2sample = new double[vers1Tests.size()];
		
		int syscallIndex = 0;
		
		for(String sysCall : keys)
		{
			int index = 0;
			for(JSONObject jsonObject: vers1Tests)
			{
				if(jsonObject.containsKey(sysCall))
					vers1sample[index++] = (long)  jsonObject.get(sysCall);
				
				else
					vers1sample[index++] = 0;
				
				//System.out.print(vers1sample[index-1]+",");
				//System.out.print( (long) jsonObject.get(sysCall));
			}
			
			//System.out.println();
			index = 0;
			for(JSONObject jsonObject: vers2Tests)
			{
				if(jsonObject.containsKey(sysCall))
					vers2sample[index++] =  (long) jsonObject.get(sysCall);
				
				else
					vers2sample[index++] = 0;
				
				//System.out.print(vers2sample[index-1]+",");
				//System.out.print((long)  jsonObject.get(sysCall));
			}
			
			double alpha = 0.1;
			
			significance[syscallIndex] = ttest(vers1sample,vers2sample,alpha);
			double pval = ttest(vers1sample,vers2sample);
		    double meanChange = mean(vers1sample)-mean(vers2sample);
		    String star = "";
		    
		    if(	pval<0.1 && pval>0.05)
		    	star="*";

		    else if(pval<=0.05 && pval>0.01)
		    	star="**";
		    
		    else if( pval<=0.01)
		    	star="***";
		    
		    
		    if(mean(vers1sample)!=0)
		    	percentageChange[syscallIndex] = 100*meanChange/mean(vers2sample);
		    
		    else
		    	percentageChange[syscallIndex] = Double.POSITIVE_INFINITY;
		    
		    if(significance[syscallIndex])
		    	sysCalls.put(sysCall+" "+star, percentageChange[syscallIndex]);
		    
		  
		    
			//System.out.println(sysCall+"------"+significance[syscallIndex]+"---- "+percentageChange[syscallIndex]+"%");
			syscallIndex++;
			
			
			
		}
		String outString="";
		
		    TreeSet<Map.Entry<String,Double>> map =  (TreeSet<Entry<String, Double>>) entriesSortedByValues(sysCalls);
		   // System.out.println(map);  
		    
		   // outString+="-----------------------------------------------------------------------\n";
		    //outString+="						REPORT:\n\n";
		    
		    if(map.size()>0)
		    	{
		    	outString+="<table id=\"t\">\n";
		    	outString+="<tr>\n"
		    	+"<th>System Call</th>\n"
		    	+"<th>%Change(in no of Calls)</th>\n"		
		    	+"<th>Description</th>\n"
		    	+"</tr>\n";
		    	}
		    
		    else
		    	outString+="No system call has changed significantly";
		    
		    String desc="";
		    for(Map.Entry<String, Double> call : map)
		    {
		    	String syscall = call.getKey().split("\\s+")[0];
		    	
		    	if(call.getValue()!=Double.POSITIVE_INFINITY)
		    	{
		    		
		    		
		    		//System.out.printf("%-30s%3.2f",call.getKey(),call.getValue());
		    		//System.out.print("%");
		    		//System.out.printf("%-30s","");
		    		if(syscallDesc.containsKey(syscall))
		    		{
		    			if(call.getValue()>0)
		    				desc=syscallDesc.get(syscall)[0];
		    			
		    			else if(call.getValue()<0)
		    				desc=syscallDesc.get(syscall)[1];
		    			
		    			desc+=" than the previous version";
		    		}
		    		
		    		else
		    			desc="No information about the call";
		    		//System.out.println();
		    		
		    		outString+="<tr>\n"
		    		    	+"<td>"+call.getKey()+"</td>\n"
		    		    	+"<td>"+String.format("%3.2f", call.getValue())+"</td>\n"		
		    		    	+"<td>"+desc+"</td>\n"
		    		    	+"</tr>\n";
		    		
		    	}
		    	
		    	else
		    		{
		    		//System.out.printf("%-30s%-37s",call.getKey(),"Absent in prior version");
		    		
		    		
		    		
		    		if(syscallDesc.containsKey(syscall))
		    			desc=syscallDesc.get(syscall)[0]+" than the previous version";
		    			
		    		else
		    			desc="No information about the call";
		    		
		    		outString+="<tr>\n"
		    		    	+"<th>"+call.getKey()+"</th>\n"
		    		    	+"<th>Absent in prior version</th>\n"		
		    		    	+"<th>"+desc+"</th>\n"
		    		    	+"</tr>\n";
		    		}
		    	
		    	
		    }
		    
		    outString+="</table>";
		    /*
		    System.out.println();
		    System.out.println();
		    System.out.println();
		    */
		    
		    if(map.size()>0)
		    {
		    	/*
		    	System.out.println("Significance of * rating:");
			    System.out.println("	* Significantly Different");
			    System.out.println("	** Highly Significantly Different");
			    System.out.println("	*** Very Very Significantly Different");
			    */
		    	
		    	outString+="<div style=\"background-color:white; color:black; margin:20px; padding:20px;\"> "
		    			+ "<h4>Significance of * rating:</h4>"
		    			+ "*&nbsp;&nbsp;&nbsp;&nbsp;Significantly Different<br>"
		    			+ "**&nbsp;&nbsp;&nbsp;Highly Significantly Different<br>"
		    			+ "***&nbsp;Very Very Significantly Different<br>"
		    			+ "</div>";
			   // System.out.println("-----------------------------------------------------------------------");
			    //System.out.println();
			    //System.out.println("Energy consumption Prediction:");
		    	//System.out.println("Your application's energy consumption seems to have changed significantly since the last recorded version");
		    	outString+="<div style=\"background-color:grey; color:white; margin:20px; padding:20px;\">"
		    			+ "<h4>Energy consumption Prediction:</h4>"
		    			+ "Your application's energy consumption seems to <b>have changed significantly</b> since the last recorded version<br>"
		    			+ "</div>";
		    	
		    	result = "Yes";
		    }
			//System.out.println(sysCalls);  
		  
		    else
		    {
		    	//System.out.println("-----------------------------------------------------------------------");
		    	outString+="<div style=\"background-color:grey; color:white; margin:20px; padding:20px;\">"
		    			+ "<h4>Energy consumption Prediction:</h4>"
		    			+ "Your application's energy consumption doesn't seem to have changed significantly since the last recorded version<br>"
		    			+ "</div>";
		    	
		    	//System.out.println("Your application's energy consumption doesn't seem to have changed significantly since the last recorded version");
		    	result = "No";
		    }	
		    /*
		    System.out.println();
		    System.out.println();
		    System.out.println();
		    System.out.println("-----------------------------------------------------------------------");
		    System.out.println();
		    System.out.println();
		    */
		   
		    //System.out.println();
		//System.out.println("keys---"+keys);
		    return outString;
		
		
	}
	
	public static double mean(double[] m) {
	    double sum = 0;
	    for (int i = 0; i < m.length; i++) {
	        sum += m[i];
	    }
	    return sum / m.length;
	}
	
	static <K,V extends Comparable<? super V>>
	SortedSet<Map.Entry<K,V>> entriesSortedByValues(Map<K,V> map) {
	    SortedSet<Map.Entry<K,V>> sortedEntries = new TreeSet<Map.Entry<K,V>>(
	        new Comparator<Map.Entry<K,V>>() {
	            @Override public int compare(Map.Entry<K,V> e1, Map.Entry<K,V> e2) {
	                return e2.getValue().compareTo(e1.getValue());
	            }
	        }
	    );
	    sortedEntries.addAll(map.entrySet());
	    return sortedEntries;
	}

}
