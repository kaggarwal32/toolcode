package strace;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class RunTestSuites {

	
	private String adbPath;
	public String testPackageName;
	public String testCodeDir;
	 
	
	RunTestSuites()
	{
		adbPath = pathVariables.adbPath;
		testCodeDir = pathVariables.testCodeDir;
		testPackageName = pathVariables.packageNameTest;
	}
	
	/*
	public static void main(String[] args)
	{
		pathVariables pb = new pathVariables();
		RunTestSuites ts = new RunTestSuites();
		callStrace cs = new callStrace();
		String straceFileOnPhone = pb.phoneDir+"trc.txt";
		try {
			cs.removeStraceFile(pb.adbPath,straceFileOnPhone);
			cs.callStrace(pb.adbPath,pb.phoneDir, pb.homeDir);
	        cs.runScript(pb.adbPath,pb.phoneDir,pb.packageName);
	       
		
	        
			ts.installAPK(ts.adbPath, ts.testCodeDir);
			
			ts.runTestInitial(ts.adbPath, ts.testPackageName);
			
			cs.getStraceFile(pb.adbPath,straceFileOnPhone,pb.homeDir);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	*/
	
	public void runTestInitial(String adbPath, String testPackageName)
	{
		ProcessBuilder pb = new ProcessBuilder(adbPath, "shell", "am", "instrument","-w", testPackageName+"/android.test.InstrumentationTestRunner");
		
		Process pc;
		try {
			pc = pb.start();
			
 		    InputStream is = pc.getInputStream();
 		    InputStreamReader isr = new InputStreamReader(is);
 		    BufferedReader br = new BufferedReader(isr);
 		    String line;	 		    
 		   
 		    while ((line = br.readLine()) != null)
 		    {    
 		    	//System.out.println(line);
 		    }
 		    
			pc.waitFor();
			
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.err.println("Error running the script");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}
	
	public void runTest(String adbPath, String testPackageName)
	{
		ProcessBuilder pb = new ProcessBuilder(adbPath, "shell", "am", "instrument","-w", testPackageName+"/android.test.InstrumentationTestRunner");
		
		Process pc;
		try {
			pc = pb.start();
			
 		    InputStream is = pc.getInputStream();
 		    InputStreamReader isr = new InputStreamReader(is);
 		    BufferedReader br = new BufferedReader(isr);
 		    String line;	 		    
 		   
 		    while ((line = br.readLine()) != null)
 		    {    
 		    	//System.out.println(line);
 		    }
 		    
			pc.waitFor();
			
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.err.println("Error running the script");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}
	
	
	public void installAPK(String adbPath, String testCodeDir)
	{
			File folder = new File(testCodeDir+"/bin/");
			File[] listOfFiles = folder.listFiles();
			String apkFile="";
			
			for (int i = 0; i < listOfFiles.length; i++) {
			      if (listOfFiles[i].isFile()) {
			    	  if(listOfFiles[i].getName().endsWith(".apk"))
			    	  {
			    		  apkFile = listOfFiles[i].getAbsolutePath();
			    		  break;
			    	  }
			      }
			    }

	    if(apkFile.isEmpty())
	    {
	    	System.out.println("Error: Android Project Test directory: apk file not present");
	    	System.exit(0);
	    }
		
	    
		ProcessBuilder pb = new ProcessBuilder(adbPath, "install",apkFile);
		
		Process pc;
		try {
			pc = pb.start();
			
			pc = pb.start();
			
 		    InputStream is = pc.getInputStream();
 		    InputStreamReader isr = new InputStreamReader(is);
 		    BufferedReader br = new BufferedReader(isr);
 		    String line;	 		    
 		   
 		    while ((line = br.readLine()) != null)
 		    {    
 		    	//System.out.println(line);
 		    }
 		    
			//pc.waitFor();
		    //pc.destroy();
		    
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.err.println("Error running the script");
			}
		
	}
	
	public List<String> getTestNames()
	{
		String path = testPackageName.replaceAll("\\.", "/");
		path = testCodeDir+"/"+path;
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();
		List<String> testNames = new ArrayList<String>();
		
		for (int i = 0; i < listOfFiles.length; i++) {
		      if (listOfFiles[i].isFile()) {
		    	  if(listOfFiles[i].getName().endsWith(".java"))
		    		  testNames.add(listOfFiles[i].getName());
		      }
		    }
		
		return testNames;
	}
}
