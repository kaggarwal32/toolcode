package strace;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class testSuiteTable {

	public void createSuiteTable()
	  {
		    Connection conn = null;
		    Statement stmt = null;
		    try {
		      Class.forName("org.sqlite.JDBC");
		      conn = DriverManager.getConnection("jdbc:sqlite:database.db");
		      //System.out.println("Opened database successfully");

		      stmt = conn.createStatement();
		      String sqlCommand = "CREATE TABLE IF NOT EXISTS SUITE " +
		                   "(TestSuiteID INTEGER PRIMARY KEY  AUTOINCREMENT," +
		    		       " Name TEXT NOT NULL)"; 
		      stmt.executeUpdate(sqlCommand);
		      stmt.close();
		      conn.close();
		      //insertSuiteTable();
		    } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);
		    }
		    
	  }
	  
	  public void insertSuiteTable(String testSuiteName)
	  {
		  Connection conn = null;
		    Statement stmt = null;
		    try {
		      Class.forName("org.sqlite.JDBC");
		      conn = DriverManager.getConnection("jdbc:sqlite:database.db");
		      conn.setAutoCommit(false);
		      //System.out.println("Opened database successfully");

		      stmt = conn.createStatement();
		      String sqlCommand = "INSERT INTO SUITE " +
		      		      " (Name) " + 
		                   "VALUES ('" +
		                   testSuiteName +
		                   "')" ; 
		      stmt.executeUpdate(sqlCommand);
		      
		      stmt.close();
		      conn.commit();
		      conn.close();
		    } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		     // System.exit(0);
		    }
	  }

}
