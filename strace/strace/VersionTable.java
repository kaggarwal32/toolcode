package strace;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class VersionTable {
	
	


	public void createVerTable()
	{
		Connection conn = null;
		Statement stmt = null;
		try {
			      Class.forName("org.sqlite.JDBC");
			      conn = DriverManager.getConnection("jdbc:sqlite:database.db");
			      //System.out.println("Opened database successfully");

			      stmt = conn.createStatement();
			      String sqlCommand = "CREATE TABLE IF NOT EXISTS VERSION " +
			                   "(ID INTEGER PRIMARY KEY  AUTOINCREMENT," +
			    		       " VersionID TEXT NOT NULL)"; 
			      stmt.executeUpdate(sqlCommand);
			      stmt.close();
			      conn.close();
			    } catch ( Exception e ) {
			      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			      System.exit(0);
			    }
			    
		  }
		  
	
	public void insertVerTable(String version)
	{
			  Connection conn = null;
			    Statement stmt = null;
			    try {
			      Class.forName("org.sqlite.JDBC");
			      conn = DriverManager.getConnection("jdbc:sqlite:database.db");
			      conn.setAutoCommit(false);
			      //System.out.println("Opened database successfully");

			      stmt = conn.createStatement();
			      String sqlCommand = "INSERT INTO VERSION " +
			      		      " (VersionID) " + 
			                   "VALUES ('" +
			                   version +"')"  ; 
			      stmt.executeUpdate(sqlCommand);
			      
			      stmt.close();
			      conn.commit();
			      conn.close();
			    } catch ( Exception e ) {
			      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			     // System.exit(0);
			   }
		 }
	
	public int isPresent(String VersID)
	  {
		    Connection conn = null;
		    Statement stmt = null;
		    int count = 0;
		    try {
		      Class.forName("org.sqlite.JDBC");
		      conn = DriverManager.getConnection("jdbc:sqlite:database.db");
		      conn.setAutoCommit(false);
		      //System.out.println("Opened database successfully");

		      stmt = conn.createStatement();
		      String sqlCommand = "SELECT COUNT(*) FROM VERSION WHERE" +
		                   " VersionID" +
		                   "=" + "'"+
		                   VersID +"'"; 
		      ResultSet result = stmt.executeQuery(sqlCommand);
		      result.next();
		      count = result.getInt(1);
		      System.out.println();
		     
		      result.close();
		      stmt.close();
		      conn.commit();
		      conn.close();
		    } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.out.println("Error");
		      System.exit(0);
		    }
		    return count;
	  }
	
	
	public int noOfVersions()
	  {
		    Connection conn = null;
		    Statement stmt = null;
		    int count = 0;
		    try {
		      Class.forName("org.sqlite.JDBC");
		      conn = DriverManager.getConnection("jdbc:sqlite:database.db");
		      conn.setAutoCommit(false);
		      //System.out.println("Opened database successfully");

		      stmt = conn.createStatement();
		      String sqlCommand = "SELECT COUNT(*) FROM VERSION"; 
		      ResultSet result = stmt.executeQuery(sqlCommand);
		      result.next();
		      count = result.getInt(1);
		      System.out.println();
		     
		      result.close();
		      stmt.close();
		      conn.commit();
		      conn.close();
		    } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.out.println("Error");
		      System.exit(0);
		    }
		    return count;
	  }
	
	
	
	public String lastVers()
	{
		
		Connection conn = null;
	    Statement stmt = null;
	    String version = "";
	    
	    try {
	      Class.forName("org.sqlite.JDBC");
	      conn = DriverManager.getConnection("jdbc:sqlite:database.db");
	      conn.setAutoCommit(false);
	      //System.out.println("Opened database successfully");

	      stmt = conn.createStatement();
	      String sqlCommand = "SELECT VersionID FROM VERSION order by ID desc limit 1"; 
	      ResultSet result = stmt.executeQuery(sqlCommand);
	      
	      result.next();
	      version = result.getString(1); 
	      
	     
	      
	      result.close();
	      stmt.close();
	      conn.commit();
	      conn.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.out.println("Error");
	      System.exit(0);
	    }
		return version;
		
	}
	
	
	public String[] lastTwoVers()
	{
		
		Connection conn = null;
	    Statement stmt = null;
	    String[] versions = new String[2];
	    
	    try {
	      Class.forName("org.sqlite.JDBC");
	      conn = DriverManager.getConnection("jdbc:sqlite:database.db");
	      conn.setAutoCommit(false);
	      //System.out.println("Opened database successfully");

	      stmt = conn.createStatement();
	      String sqlCommand = "SELECT VersionID FROM VERSION order by ID desc limit 2"; 
	      ResultSet result = stmt.executeQuery(sqlCommand);
	      
	      result.next();
	      versions[0] = result.getString(1); 
	      
	      result.next();
	      versions[1] = result.getString(1); 
	      
	      result.close();
	      stmt.close();
	      conn.commit();
	      conn.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.out.println("Error");
	      System.exit(0);
	    }
		return versions;
		
	}

}


