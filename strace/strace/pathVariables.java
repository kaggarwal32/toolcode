package strace;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;



public class pathVariables {

	public static String gitDir;
	public static String appCodeDir;
	public static String testCodeDir;
	public static String homeDir;
    public static final String phoneDir = "/data/local/";
    public static String packageName;
    public static String packageNameTest;
    public static String straceFile;
    public static String adbPath;
    public static final int minTests = 5;
    
    pathVariables(){
    	try {
			readVariables();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
	public void readVariables() throws IOException
	{
		 File filePath = new File(System.getProperty("user.dir"));
		 String variableFile = filePath.getParent().toString()+"/setVariables";
		 homeDir = filePath.getParent().toString()+"/stracing/";
		 
		 BufferedReader br = null;
		 
			try {
	 
				
	 
				br = new BufferedReader(new FileReader(variableFile));
	 
				adbPath = br.readLine().split("=\\s*")[1];
				gitDir = br.readLine().split("=\\s*")[1];
				appCodeDir= br.readLine().split("=\\s*")[1];
				testCodeDir = br.readLine().split("=\\s*")[1];
				packageName = getPackageName();
				packageNameTest = getTestPackageName();
				
				/*
				System.out.println(adbPath);
				System.out.println(gitDir);
				System.out.println(appCodeDir);
				System.out.println(testCodeDir);
		 		*/
				//System.out.println(packageName);
		 
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (br != null)br.close();
				} catch (IOException ex) {
					System.out.println("Please check the variables file");
					ex.printStackTrace();
				}
			}
	}
	
	
	public String getPackageName()
	{

		BufferedReader br = null;
		String nextLine;
		String xmlFile=null;
		try {
			br = new BufferedReader(new FileReader(appCodeDir+"/AndroidManifest.xml"));
			while((nextLine = br.readLine()) != null)
			{
				xmlFile+=nextLine;
			}
			
			String packageN = xmlFile.split("package\\s*=\\s*\"")[1];
			return packageN.substring(0, packageN.indexOf("\""));
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public String getTestPackageName()
	{

		BufferedReader br = null;
		String nextLine;
		String xmlFile=null;
		try {
			br = new BufferedReader(new FileReader(testCodeDir+"/AndroidManifest.xml"));
			while((nextLine = br.readLine()) != null)
			{
				xmlFile+=nextLine;
			}
			
			String packageN = xmlFile.split("package\\s*=\\s*\"")[1];
			return packageN.substring(0, packageN.indexOf("\""));
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
}
