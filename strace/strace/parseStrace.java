package strace;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;
import java.util.TreeMap;
import org.json.simple.JSONObject;


public class parseStrace {

	public String parseStrace(String straceFile) throws IOException{
		
		Map<String, Integer> sysCalls = new TreeMap<String, Integer>();
		
		BufferedReader br = new BufferedReader(new FileReader(straceFile));
		String syscallData;
		String jsonText = null;
		
		try{
			br.readLine();
			br.readLine();
			while((syscallData = br.readLine()) != null)
			{
			 String[] tuple = syscallData.split("\\s+");
				
			 if(!tuple[0].startsWith("Sys"))
			 {
				 if(!tuple[0].startsWith("%"))
				 {
					 if(!tuple[0].startsWith("100.00"))
					 {
						 if(!tuple[tuple.length-1].equals("total"))
						 {
							if(!tuple[2].contains("-"))
							{
								sysCalls.put(tuple[tuple.length-1],Integer.parseInt(tuple[4]));
							}		
							 
						 }
					 }
				 }
			 }
					
			else if(tuple[tuple.length-1].equals("total"))
			{
				sysCalls.put(tuple[tuple.length-1],Integer.parseInt(tuple[2]));
			

			 }
			 
			else if(tuple[0].startsWith("100.00"))
			{
				sysCalls.put(tuple[tuple.length-1],Integer.parseInt(tuple[3]));
			 

			 }
					
		 }
		 br.close();
				
		 JSONObject callDataJSON = new JSONObject();

			for(String callName : sysCalls.keySet())
			{
				int noCalls = sysCalls.get(callName);
				callDataJSON.put(callName, noCalls);
			}

	      StringWriter callDataString = new StringWriter();
	      callDataJSON.writeJSONString(callDataString);
	      
	      jsonText = callDataString.toString();
				
				
	     }catch(Exception ex)
	     {
			    	System.out.println("Error: ");
			    	ex.printStackTrace();
	     }
		
			
		
		return 	jsonText;
			
	}
			
	
	
	public static  int calculateMedian(int[] array)
	{
		int max = array.length;
		 for(int i = 1; i < max; i++)
         {
               for(int j = 0; j < max - i; j++)
               {
                     if(array[j] > array[j + 1])
                     {
                           int temp = array[j];
                           array[j] = array[j + 1];
                           array[j + 1] = temp;
                     }
               }
         }
		 
		 return array[max/2];
		
	}
	
	public static  double calculateMean(int[] array)
	{
		int max = array.length;
		double mean =0;
		 for(int i = 1; i < max; i++)
         {
               
                           mean+= array[i];
                           
                    
         }
		 mean = mean/max;
		 
		 return mean;
		
	}
}