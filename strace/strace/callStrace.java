package strace;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class callStrace {
	public static void main(String[] args) throws IOException{
		
		String version1 = System.getProperty("versOne");
		String version2 = System.getProperty("versTwo");
		
		if(version1!=null && version2!= null)
			System.out.println("Running: "+version1+":"+version2);
		
		pathVariables path = new pathVariables();
		
		pathVariables.straceFile = pathVariables.homeDir+"trc.txt";
		
		String adbPath = pathVariables.adbPath;
		String homeDir = pathVariables.homeDir;
		String phoneDir = pathVariables.phoneDir;
		String straceFileOnPhone = phoneDir+"trc.txt";
		String packName = pathVariables.packageName;
		 
        ///*
		database db = new database();
		//for(int i=0; i<2;i++)
			//db.lastId();
		
		 
		 int prvsRuns = db.countVersionTests(getGitData.getRepoVersionId(new File(pathVariables.gitDir)));
		 
		 
		 VersionTable vt = new VersionTable();
		 int noVers = vt.noOfVersions();
		 
		 int runsReq = pathVariables.minTests - prvsRuns;
		 int runNo = prvsRuns+1;
		
		if(runsReq>0)
		{
			while(runsReq-- > 0)
			{
				
				removeStraceFile(adbPath,straceFileOnPhone);
		        callStrace(adbPath,phoneDir, homeDir);
		        
		        if(runScript(adbPath,phoneDir,packName))
		        	 System.out.println("Running Test Run"+runNo);
		        
		        RunTestSuites rts = new RunTestSuites();
		        rts.installAPK(adbPath, rts.testCodeDir);
		        rts.runTestInitial(adbPath, rts.testPackageName);
		        
		        try {
					Thread.sleep(20000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
		        
		        if(!getStraceFile(adbPath,straceFileOnPhone,homeDir))
		        {
		        	runsReq++;
		        	continue;
		        }
				String straceFile = pathVariables.straceFile;
				
				parseStrace json = new parseStrace();
				
				
				
				//List<String> ls = db.getVersionTests(getGitData.getRepoVersionId(new File(pathVariables.gitDir)));
				//System.out.println(ls.get(0));
				String jsonBlob = json.parseStrace(straceFile);
				
				if(!jsonBlob.isEmpty())
					db.dbInsert(jsonBlob);
				else
				{
					System.out.println("This run failed, please run the application again.");
					continue;
				}
				
				//System.out.println(db.countVersionTests(getGitData.getRepoVersionId(new File(pathVariables.gitDir))));
				//System.out.println(getGitData.getRepoVersionId(new File(pathVariables.gitDir)));
				//System.out.println(json.parseStrace(straceFile));
				
				runNo++;
				}
			}
			
			if(noVers<1)
				System.out.println("No Previous versions present, so can't compare");
			
			else if(noVers>0)
			{
				String[] vers = vt.lastTwoVers();
				htmlWriter htm = new htmlWriter();
				
			    tTest ttest = htm.htmlGen(vers[0], vers[1]);
				experimentsResults exp = new experimentsResults();
			    exp.insertResultsTable(vers[0], vers[1], ttest.result);
			    
			}
			
		
		}
	
	public static boolean getStrace( String abdPath, String straceFileOnPhone, String homeDir) throws IOException
	{
		if(straceFileExists( abdPath,  straceFileOnPhone))
		{
			ProcessBuilder pb = new ProcessBuilder(abdPath, "pull", straceFileOnPhone, homeDir);
			Process pc = pb.start();
			
			try {
				pc.waitFor();
				pc.destroy();
			
		
			} catch (InterruptedException e) {
				System.out.println("Error transfering files: "+e.getMessage());
				e.printStackTrace();
			}
			}
			
			else
			{
				//System.out.println("Error--- Please restart the Program");
				return false;
			}
			
		return true;
	}
	
	public static boolean getStraceFile( String abdPath, String straceFileOnPhone, String homeDir) throws IOException
	{
		
		ProcessBuilder pb = new ProcessBuilder(abdPath, "shell","ls","-l", straceFileOnPhone);
		boolean exists = false;
		
		Process process;
		try {
				process = pb.start();
			
		    InputStream is = process.getInputStream();
		    InputStreamReader isr = new InputStreamReader(is);
		    BufferedReader br = new BufferedReader(isr);
		    String line;
		    while ((line = br.readLine()) != null) 
		    {
		    	 if(line.endsWith("trc.txt"))
			     {  
			    	 exists = true;
			    	 break;
			     }
			     
			     
		    }
		    process.destroy();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		if(exists==false)
			return false;
			
			
		ProcessBuilder pbl = new ProcessBuilder(abdPath, "pull", straceFileOnPhone, homeDir);
		Process pc = pbl.start();
				
			try {
				pc.waitFor();
				pc.destroy();
			
		
			} catch (InterruptedException e) {
				System.out.println("Error transfering files: "+e.getMessage());
				e.printStackTrace();
			}
			
			
			
		return true;
	}
	
	public static boolean straceFileExists(String abdPath, String straceFileOnPhone)
	{
		ProcessBuilder pb = new ProcessBuilder(abdPath, "shell","ls","-l", straceFileOnPhone);
		
		boolean exists = true;
		/*try {
			Thread.sleep(20000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}*/
		
		try {
 			
 			boolean wait = true;
 			boolean first = false;
 			
 			ProcessBuilder builder2 = new ProcessBuilder(abdPath, "shell","ps | grep -w",pathVariables.packageName);
 			String RorS = "";   
 			String line2;
 			String lastLineSeen = "";
 			
 			while(wait)
 			{
 				//wait = false;
	 		    Process process2 = builder2.start();
	 		    InputStream is2 = process2.getInputStream();
	 		    InputStreamReader isr2 = new InputStreamReader(is2);
	 		    BufferedReader br2 = new BufferedReader(isr2);
	 		    	 		    
	 		    boolean seen = false;
	 		    while ((line2 = br2.readLine()) != null)
	 		    {    //System.out.println(line2);
	 			     if(line2.endsWith(pathVariables.packageName))
	 			     {   
	 			    	 
	 			    	 first = true;
	 			    	 seen = true;
	 			    	 lastLineSeen = line2;
		 			     /*
	 			    	 try {
				 				Thread.sleep(8000);
		 			     } catch (InterruptedException e1) {
				 				e1.printStackTrace();
				 		 }
		 			     */
	 			    	 break;
	 			     }
	 			     
	 			     
	 		    }
	 		    
	 		    if(first && !seen )
	 		    {
		 		      wait = false;
		 		      
		 		     /* 
		 		     String[] cond = lastLineSeen.split("\\s+");
		 		     //System.out.println(lastLineSeen+"---"+cond[cond.length-2]);
		 		     if(cond[cond.length-2].equals("S"))
		 		     {
		 		    	wait = true;
		 		    	first = false;
		 		    	//System.out.println("Enetered loop");
		 		    	removeStraceFile(pathVariables.adbPath,straceFileOnPhone);
				        callStrace(pathVariables.adbPath,pathVariables.phoneDir, pathVariables.homeDir);
				        runScript(pathVariables.adbPath,pathVariables.phoneDir,pathVariables.packageName);
		 		     }
		 		     */
	 		    }
	 		    
	 		    process2.destroy();
 			}
 			
 		} catch (IOException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
		
		try {
				Thread.sleep(4000);
	     } catch (InterruptedException e1) {
				e1.printStackTrace();
		 }
		
		
		while(exists)
		{
			
			
		    Process process;
			try {
				process = pb.start();
			
		    InputStream is = process.getInputStream();
		    InputStreamReader isr = new InputStreamReader(is);
		    BufferedReader br = new BufferedReader(isr);
		    String line;
		    while ((line = br.readLine()) != null) 
		    {
		    	 //System.out.println(line);
			     if(line.endsWith("trc.txt"))
			     {   
			    	 exists = false;
			    	 
			    	 
			    	 
			    	 break;
			    	 
			     }
			     
			     else
			     {
			    	 System.out.println("This run failed, please run the application again.");
			    	 return false;
			     }
		    }
		    process.destroy();
			} catch (IOException e) {
				e.printStackTrace();
			}
			/*
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			*/
		}
		
		return !exists;
	}
	
	public static void removeStraceFile( String abdPath, String straceFileOnPhone) throws IOException
	{
		//straceFileOnPhone = straceFileOnPhone+"trc.sh";
	
		ProcessBuilder pb = new ProcessBuilder(abdPath, "shell","rm", straceFileOnPhone);
		Process pc = pb.start();
		
		try {
			pc.waitFor();
			pc.destroy();
		//System.out.println("Done");
	
		} catch (InterruptedException e) {
			System.out.println("Error transfering files: "+e.getMessage());
			e.printStackTrace();
		}
	}
	
	public static void callStrace( String abdPath, String outputfile, String homeDir) throws IOException
	{
		String scriptFile = homeDir+"script.sh";
		
		ProcessBuilder pb = new ProcessBuilder(abdPath, "push", scriptFile, outputfile);
		Process pc = pb.start();
		//pc.destroy();
		try {
			pc.waitFor();
			pc.destroy();
			String straceFile = homeDir+"strace";
			ProcessBuilder pb2 = new ProcessBuilder(abdPath, "push", straceFile, outputfile);
			Process pc2 = pb2.start();
			pc2.waitFor();
			pc2.destroy();
			
			String strchFile = homeDir+"strc.sh";
			ProcessBuilder pb3 = new ProcessBuilder(abdPath, "push", strchFile, outputfile);
			Process pc3 = pb3.start();
			pc3.waitFor();
			pc3.destroy();
			
			
		} catch (InterruptedException e) {
			System.out.println("Error transfering files: "+e.getMessage());
			e.printStackTrace();
		}
	}
	
	public static boolean runScript( String abdPath, String phoneDir, String packName) 
	{
		String scriptPath = phoneDir+"strc.sh";
		boolean bn = true;
		
	    while(bn)
	    {
			ProcessBuilder pb = new ProcessBuilder(abdPath, "shell","sh", scriptPath,packName,"&");
			
			Process pc;
			try {
				pc = pb.start();
			
				
			    ProcessBuilder builder = new ProcessBuilder(abdPath, "shell","ps | grep -w sh");
			    
			    
			    Process process = builder.start();
			    InputStream is = process.getInputStream();
			    InputStreamReader isr = new InputStreamReader(is);
			    BufferedReader br = new BufferedReader(isr);
			    String line;
			    while ((line = br.readLine()) != null) {
				     if(line.endsWith(" sh") && line.startsWith("root"))
				     { 
				    	
				    	 bn = false;
				    	 return true;
				    	 
				     }
			    }
			    
			    process.destroy();
			    
				 if(bn)
					 pc.destroy();
				//pc.waitFor();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.err.println("Error running the script");
				}
			
		    }
	    return true;
	    
	}
	
	
}
