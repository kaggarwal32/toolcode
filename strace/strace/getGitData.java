package strace;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.errors.NoWorkTreeException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

public class getGitData {

	/**
	 * @param args
	 * @throws GitAPIException 
	 * @throws NoHeadException 
	 */
	
	
	public static void getGitData() throws NoHeadException, GitAPIException {
		FileRepositoryBuilder builder = new FileRepositoryBuilder();
		
		File gitDir = new File(pathVariables.gitDir);
		String appCodeDir = pathVariables.appCodeDir;
		
		//Git git = Git.open(gitDir);
		
		getRepoVersionId(gitDir);
		if(isDirty(gitDir,appCodeDir))
			System.out.println("Is dirty");
		
		List<String> javaTestFiles = new ArrayList<String>();
		File dir = new File("/home/karan/repos/dedup-icsme/paper");
		File[] files = dir.listFiles();
		for(File file : files)
		{
			if(file.toString().endsWith(".java"))
			{
				javaTestFiles.add(file.getName());
				System.out.println(file.getName());
			}
		}
		
		String[] testJavaFiles = new String[javaTestFiles.size()];
		testJavaFiles = javaTestFiles.toArray(testJavaFiles);
		
		for(String file : testJavaFiles)
		{
			System.out.println(file);
		}
		//System.out.println(git.log());

	}
	
	public static String getRepoVersionId(File repoPath)
	{
        Git git;
		try {
			git = Git.open(repoPath);
			Iterator<RevCommit> iterator = git.log().call().iterator();
		    RevCommit rc2 = iterator.next();
		    git.close();
		    return rc2.getName();
		      
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoHeadException e) {
			
			e.printStackTrace();
		} catch (GitAPIException e) {
			
			e.printStackTrace();
		}
      return null;
	}

	
	public static boolean isDirty(File repoPath, String appCodePath)
	{
		
		Git git;
		try {
			git = Git.open(repoPath);
			Status status=git.status().call();
		     //System.out.println();
		     //appCodePath.substring(appCodePath.substring(0, appCodePath.length()-2).lastIndexOf("/", 0))
			 
		     if(!status.getModified().isEmpty())
		    	 for(String modified:status.getModified())
		    	 {		System.out.println(modified);
		    		 	if(modified.contains(appCodePath.substring(appCodePath.substring(0, appCodePath.length()-1).lastIndexOf("/")+1)))
		    		 			if(modified.endsWith(".java"))
		    		 				return true;     
		    	 }
		      git.close();
		        
		} catch (IOException e) {
			 
			e.printStackTrace();
			
		} catch (NoWorkTreeException e) {
			
			e.printStackTrace();
			
		} catch (GitAPIException e) {

			e.printStackTrace();
		}
	        
		 
	     return false;  
	}
	
	
	public static String diffVersions(String vers1, String vers2,String repoPath)
	{
		
		File gitDir = new File(repoPath+"/.git");
		Repository repository ;
		String outText = "";
		 try {
			repository = new FileRepository(gitDir);
			
		//ObjectId head = repository.resolve("HEAD^{tree}");
		
		  try {
                Git git = Git.open(new File(repoPath));
     			Iterator<RevCommit> iterator = git.log().call().iterator();
     			RevCommit ver1 = null,ver2 = null;
     			git.close();
     			
     		    RevCommit rc2;
     		    boolean v1 = false, v2 = false;
     		    
     		    
     		    while((rc2=iterator.next())!=null)
     		    {
     		    
	     		     if(rc2.getName().equals(vers1 ))
	     		     {
	     		    	 ver1 = rc2;
	     		    	  v1 = true;
	     		     }
	     		     
	     		     else if(rc2.getName().equals(vers2 ))
	     		     { 
	     		    	 ver2 =rc2;
	     		    	 v2 = true;
	     		     }
     		     
     		     if(v1 && v2)
     		    	 break;
     		     
     		    }

                 if(!v1)
                 {
                	 return "Version: "+vers1+" not present in Git, please check the Git path";
                	 //System.exit(0);
                 }

                 if(!v2)
                 {
                	 return "Version: "+vers2+" not present in Git, please check the Git path";
                	 //System.exit(0);
                 }
                 
               
                 
     		   ByteArrayOutputStream out = new ByteArrayOutputStream();
  		       DiffFormatter df = new DiffFormatter(out);
  		       df.setRepository(repository);

     		      List<DiffEntry> diffs = df.scan(ver2.getTree(), ver1.getTree());
     		      
     		      for (DiffEntry diff : diffs) {
     		        
     		    	  df.format(diff);
     		    	  String diffText = out.toString("UTF-8");

     		    	  out.reset();
     		    	  if(diff.getNewPath().toString().endsWith(".java"))
     		    		 outText+="\n<b>"+diffText.split("\\n", 2)[0]+"</b>\n"+diffText.split("\\n", 2)[1];
     		      }
		  }catch(Exception e)
		  {
			  e.printStackTrace();
		  }

		 } catch (IOException e1) {
				// TODO Auto-generated catch block
		 	e1.printStackTrace();
		}
		 return outText;
	}
	
	public static boolean isSuiteChanged(File repoPath, String appCodePath)
	{
		
		Git git;
		try {
			git = Git.open(repoPath);
			Status status=git.status().call();
		     //System.out.println();
		     //appCodePath.substring(appCodePath.substring(0, appCodePath.length()-2).lastIndexOf("/", 0))
			 
		     if(!status.getModified().isEmpty())
		    	 for(String modified:status.getModified())
		    	 {		System.out.println(modified);
		    		 	if(modified.contains(appCodePath.substring(appCodePath.substring(0, appCodePath.length()-1).lastIndexOf("/")+1)))
		    		 			if(modified.endsWith(".java"))
		    		 				return true;     
		    	 }
		      git.close();
		        
		} catch (IOException e) {
			 
			e.printStackTrace();
			
		} catch (NoWorkTreeException e) {
			
			e.printStackTrace();
			
		} catch (GitAPIException e) {

			e.printStackTrace();
		}
	        
		 
	     return false;  
	}
	
	
}