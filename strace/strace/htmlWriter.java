package strace;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;

public class htmlWriter {
	
	public tTest htmlGen(String vers1, String vers2)
	{
		 
		/* StringWriter stringWriter = new StringWriter();
	       PrintWriter printWriter = new PrintWriter(stringWriter);
	       //DecimalFormat df = new DecimalFormat("###.##");
	       //printWriter.print(df.format(2.4343));
	       printWriter.print(String.format("%-30s%3.2f","fdsfdsfs",2.4343));
	       printWriter.println("second thing");
	       // ... several things
	       printWriter.println("last thing");
	       printWriter.flush();
	       printWriter.close();
	        System.out.println(stringWriter.toString()); */
		
		tTest ttest = new tTest(vers1, vers2);
		
		String htmlString = htmlWriter.htmlString;
		String reportString = ttest.getReport();
		String gitDiff= getGitData.diffVersions(vers1, vers2, pathVariables.gitDir);
		
		htmlString = htmlString.replace("$title", "Report");
		htmlString = htmlString.replace("$body", reportString);
		htmlString = htmlString.replace("$dif", "<div class=\"dot\">Application Code Change:<br>"+gitDiff.replaceAll("\\n", "<br>").
				replaceAll("(\\<br\\>\\+)(.*?)(\\<br\\>)", "<br><span style=\"background-color:#9AFF9A;\">+$2</span><br>").
				replaceAll("(\\<br\\>\\-)(.*?)(\\<br\\>)", "<br><span style=\"background-color:#FFCCE6;\">-$2</span><br>")+"</div>");

		
		
		File file = new File(pathVariables.homeDir+"/report.html");

		
		try {
			PrintWriter out = new PrintWriter(pathVariables.homeDir+"/report.html");
			out.println(htmlString);

			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		return ttest;
		
	}
	
	private final static String htmlString = "<!DOCTYPE html>\n<html>\n"
			+ "<head>"
	+"<title>$title</title>"
	+ "<style>"
	+ "body {background-color:white}"
  + "h1   {color:black}"
  + "p    {color:green}\n" +
  "table {" +
  "width:100%; " +
  "}\n" +
  "table, th, td {" +
  "border: 1px solid black;" +
  "border-collapse: collapse;" +
  "}\n" +
  "th, td {" +
  "padding: 5px;" +
  "text-align: left;" +
  "}\n" +
  "table#t tr:nth-child(even) {" +
  "background-color:#eee;" +
  "}\n" +
  "table#t tr:nth-child(odd) {" +
  "background-color:#fff;" +
  "}\n" +
  "table#t th {" +
  "background-color: blue;" +
  "color: white;" +
  "}\n"
     + "div.dot {width: 100%;background:white;border:1px dotted black;padding:8px;}\n"
    + "</style>\n"
	+"</head>"
	+"<body><h1 align=\"center\">REPORT</h1>$body $dif"
	+"</body>"
	+"</html>";
	
}
