package strace;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class experimentsResults {

	
	public void createResultsTable()
	  {
		    Connection conn = null;
		    Statement stmt = null;
		    try {
		      Class.forName("org.sqlite.JDBC");
		      conn = DriverManager.getConnection("jdbc:sqlite:database.db");
		      //System.out.println("Opened database successfully");

		      stmt = conn.createStatement();
		      String sqlCommand = "CREATE TABLE IF NOT EXISTS EXPERIMENTS " +
		                   "(ID INTEGER PRIMARY KEY  AUTOINCREMENT," +
		    		       " VERSION_1 TEXT NOT NULL,VERSION_2 TEXT NOT NULL, RESULT TEXT NOT NULL)"; 
		      stmt.executeUpdate(sqlCommand);
		      stmt.close();
		      conn.close();
		      
		    } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);
		    }
		    
	  }
	  
	  public void insertResultsTable(String vers1, String vers2, String result)
	  {
		  Connection conn = null;
		    Statement stmt = null;
		    try {
		      Class.forName("org.sqlite.JDBC");
		      conn = DriverManager.getConnection("jdbc:sqlite:database.db");
		      conn.setAutoCommit(false);
		      //System.out.println("Opened database successfully");

		      stmt = conn.createStatement();
		      String sqlCommand = "INSERT INTO EXPERIMENTS " +
		      		      " (VERSION_1,VERSION_2,RESULT) " + 
		                   "VALUES ('" + vers1 +"','"+
		                    vers2 +"','"+ 
		                    result+
		                   "')"  ; 
		      stmt.executeUpdate(sqlCommand);
		      
		      stmt.close();
		      conn.commit();
		      conn.close();
		    } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		     // System.exit(0);
		    }
	  }

}
