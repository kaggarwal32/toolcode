package strace;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import org.sqlite.SQLiteConfig;

public class database
{
	
  database()
  {
	  
	  createCallTable();
	  testSuiteTable test = new testSuiteTable();
      test.createSuiteTable();
      
      VersionTable verTable = new VersionTable();
      verTable.createVerTable();
      
      experimentsResults exp = new experimentsResults();
      exp.createResultsTable();

      
       
  }
  
  
  public void dbInsert(String sysRecord)
  {
    //Connection c = null;
    try {
      Class.forName("org.sqlite.JDBC");
      //c = DriverManager.getConnection("jdbc:sqlite:database.db");
     
      String versId=getGitData.getRepoVersionId(new File(pathVariables.gitDir));
      VersionTable verT = new VersionTable();
      
      if(verT.isPresent(versId)==0)
    	  verT.insertVerTable(versId);
      
      insertCallTable(versId, 1, sysRecord, 1);
      //Statement stmt = null;
      //stmt = c.createStatement();
      //ResultSet rs = stmt.executeQuery( "SELECT * FROM SUITE" );
     
      //rs.next();
      //System.out.println(rs.getString(2));
    } catch ( Exception e ) {
      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
      System.exit(0);
    }
    //System.out.println("Opened database successfully");
  }
  
  private void createCallTable()
  {
	    Connection conn = null;
	    Statement stmt = null;
	    try {
	      Class.forName("org.sqlite.JDBC");
	      SQLiteConfig config = new SQLiteConfig();  
	      config.enforceForeignKeys(true); 
	      conn = DriverManager.getConnection("jdbc:sqlite:database.db",config.toProperties());
	      //System.out.println("Opened database successfully");
	     
	      stmt = conn.createStatement();
	      String sqlCommand = "CREATE TABLE IF NOT EXISTS TESTS " +
	                   "(TestID INTEGER PRIMARY KEY  AUTOINCREMENT," +
	                   " VersID TEXT NOT NULL, " + 
	                   " TestSuiteID  INT NOT NULL, " + 
	                   " Syscalls  BLOB NOT NULL, " + 
	                   " Dirty INT NOT NULL," +
	                   " TIME TEXT NOT NULL," +
	                   "FOREIGN KEY(TestSuiteID) REFERENCES SUITE(TestSuiteID))"; 
	      stmt.executeUpdate(sqlCommand);
	      stmt.close();
	      conn.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
	    
  }
  
  private void insertCallTable(String VersID, int TestSuiteID, String callsData, int Dirty)
  {
	  Connection conn = null;
	    Statement stmt = null;
	    try {
	      Class.forName("org.sqlite.JDBC");
	      conn = DriverManager.getConnection("jdbc:sqlite:database.db");
	      conn.setAutoCommit(false);
	      //System.out.println("Opened database successfully");

	      stmt = conn.createStatement();
	      String sqlCommand = "INSERT INTO TESTS " +
	                   "(" +
	                   " VersID, " + 
	                   " TestSuiteID, " + 
	                   " Syscalls, " + 
	                   " Dirty," +
	                   " TIME)" +
	                   " VALUES('" + VersID + "','" +
	                     TestSuiteID + "','" +
	                     callsData + "','" +
	                     Dirty + "'," +
                         " DateTime('now') )"  ; 
	      int val = stmt.executeUpdate(sqlCommand);
	      //System.out.println(val);
	     
	      stmt.close();
	      conn.commit();
	      conn.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.out.println("Error");
	      System.exit(0);
	    }
  }
  
  public int countVersionTests(String VersID)
  {
	    Connection conn = null;
	    Statement stmt = null;
	    int count = 0;
	    try {
	      Class.forName("org.sqlite.JDBC");
	      conn = DriverManager.getConnection("jdbc:sqlite:database.db");
	      conn.setAutoCommit(false);
	      //System.out.println("Opened database successfully");

	      stmt = conn.createStatement();
	      String sqlCommand = "SELECT COUNT(*) FROM TESTS WHERE" +
	                   " VersID" +
	                   "=" + "'"+
	                   VersID +"'"; 
	      ResultSet result = stmt.executeQuery(sqlCommand);
	      result.next();
	      count = result.getInt(1);
	      System.out.println();
	     
	      result.close();
	      stmt.close();
	      conn.commit();
	      conn.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.out.println("Error");
	      System.exit(0);
	    }
	    return count;
  }
  
  public List<String> getVersionTests(String VersID)
  {
	    Connection conn = null;
	    Statement stmt = null;
	    List<String> syscallRecords = new ArrayList<String>();
	    
	    try {
	      Class.forName("org.sqlite.JDBC");
	      conn = DriverManager.getConnection("jdbc:sqlite:database.db");
	      conn.setAutoCommit(false);
	      //System.out.println("Opened database successfully");

	      stmt = conn.createStatement();
	      String sqlCommand = "SELECT Syscalls FROM TESTS WHERE" +
	                   " VersID" +
	                   "=" + "'"+
	                   VersID +"'"; 
	      ResultSet result = stmt.executeQuery(sqlCommand);
	      
	      while(result.next())
	      {
	    	  syscallRecords.add(result.getString(1)); 
	      }
	      result.close();
	      stmt.close();
	      conn.commit();
	      conn.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.out.println("Error");
	      System.exit(0);
	    }
		return syscallRecords;
  }
  
  
  public void lastId()
  {
	    Connection conn = null;
	    Statement stmt = null;
	   
	    
	    try {
	      Class.forName("org.sqlite.JDBC");
	      conn = DriverManager.getConnection("jdbc:sqlite:database.db");
	      conn.setAutoCommit(false);
	      //System.out.println("Opened database successfully");

	      stmt = conn.createStatement();
	      String sqlCommand = "DELETE FROM TESTS WHERE TestID = (SELECT MAX(TestID) FROM TESTS)"; 
	      stmt.executeUpdate(sqlCommand);
	      stmt.close();
	      conn.commit();
	      conn.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.out.println("Error");
	      System.exit(0);
	    }
		
  }
}