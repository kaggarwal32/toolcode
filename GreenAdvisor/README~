This document explains the requirements and working of Green Advisor. 


This is a Java based tool and has been tested on Ubuntu, Windows, and MacOS platforms.
Green Advisor advises Android Application developers on Change in energy consumption 
of their app with change in app code, based on system call tracing. 

-----------CONFIGURATION-----------

Please extract the tool zip in your Github project directory. 

a) Green Advisor needs four paths to be set up in order to work. These are:

	1) adbPath: The path to ADB(Android Debug Bridge) that comes with your adt bundle 
	and would be Android SDK folder's platform tools.
	Eg: adbPath=/home/karan/adt-bundle/sdk/platform-tools/adb

	2) gitDir: This is the path to your Github directory you are using for the Android Application 
	project. 
	Eg: gitDir=/home/karan/repos/CalculatorApp/

	3) appCodeDir: This is the path to the directory containing your Application code(i.e. where
	your App's AndroidManifest.xml is stored). It may or may not be same as your Github directory.
	Eg: appCodeDir=/home/karan/repos/CalculatorApp/CalcApp/
	or, appCodeDir=/home/karan/repos/CalculatorApp/

	4) testCodeDir: This is the path to the directory containing your Android Junit tets code(i.e. where
	your Jnuit test's AndroidManifest.xml is stored). It would be some where in your Github directory. 
	Eg: appCodeDir=/home/karan/repos/CalculatorApp/CalcTest/

b) The system should have a valid Java installation.

So extract the tool in the Folder with path of gitDir. So, you will have a folder with the path:
from above eg. /home/karan/repos/CalculatorApp/GreenAdvisor


-----------HOW TO RUN------------

1. Please have your Android emulator **running** before starting the Green Advisor.
2. Once you have set up the Configuration, open Terminal/CMD, and then go to the jar directory in the
Green Advisor directory.

Eg: cd /home/karan/repos/CalculatorApp/GreenAdvisor/jar/

3. Then run the following command:
   java -cp tool.jar strace.callStrace

This will execute the Green Advisor that will run your Junit tests on the Application.
If being run for the first time it will simply tell that it has no history of your App,
and hence, will not be able to give any recommendation.

When you change your application next time, and have commited it to your repository,
you can use Green Advisor's recommendation about your Application's energy change
by following the same above steps to run the tool. It will generate a HTML report 
in the Green Advisor Directory, i.e. a file named:
/home/karan/repos/CalculatorApp/GreenAdvisor/Report.html

This report will give the system call change data, and list the calls that have changed
significantly with their operations. It then gives the recommendation whether the 
application's energy consumption has changed or not. It also displays the Git code 
diff between two versions.

Please note: 
It only shows the difference between the last recorded version with the Green Advisor
and the latest version on the Git repo; it won't  show any data for the 
intermediate versions.

------------OPTIONS-------------
To run and compare the difference between two versions identified by their
Commit SHAs:
Run the following command in the Step 3 of the How to run section:
    java -DversOne=CommitSHAofVers1 -DversTwo=CommitSHAofVers2  -cp tool.jar strace.callStrace 

eg: java -DversOne=2de3hfi5sn3v3ewd2 -DversTwo=e2e2b2ue24nefi3i -cp tool.jar strace.callStrace  

This will generate the report based on these two versions.


-------------TROUBLESHOOTING------

1. Check whether the paths in the SetVariables file are set correctly.
2. You might be running the tool without having any Android Junit Tests.
3. Check whether the Jnuit Project has the .apk file in the bin/ folder.




------------LICENSE---------------

(c) 2014 Karan Aggarwal 

Under LGPL Version 3, please see LICENSE
